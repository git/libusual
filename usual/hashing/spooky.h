
#ifndef _USUAL_HASHING_SPOOKY_H_
#define _USUAL_HASHING_SPOOKY_H_

#include <usual/base.h>

void spookyhash(const void *message, size_t length, uint64_t *hash1, uint64_t *hash2);

#endif

